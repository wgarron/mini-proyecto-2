export  interface Expense {
  id?: number;
  category: string;
  expenseName: string;
  expenseAmount: number;
}

export  interface Budget {
  id?: number;
  budgetAmout: number;
}
