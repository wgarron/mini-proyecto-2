import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Expense, Budget } from "../shared/expenses";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

@Injectable({
  providedIn: 'root'
})
export class ExpensesService {
   private api_url = 'http://localhost:8000/all_expenses';
   private api_url_budget = 'http://localhost:8000/budget';

  constructor(private http: HttpClient) {}

  //get expenses and budget
  get_expenses(): Observable<Expense[]> {
    return this.http.get<Expense[]>(this.api_url);
  }

  get_budget(): Observable<Budget[]> {
    return this.http.get<Budget[]>(this.api_url_budget);
  }

  get_expense_by_id(id: number): Observable<Expense> {
    const url = `${this.api_url}/${id}`;
    return this.http.get<Expense>(url);
  }

  get_budget_by_id(id: number): Observable<Budget> {
    const url = `${this.api_url}/${id}`;
    return this.http.get<Budget>(url);
  }

  //delete expenses and budget
  delete_expense(expense: Expense): Observable<Expense> {
    const url = `${this.api_url}/${expense.id}`;
    return this.http.delete<Expense>(url);
  }

   delete_budget(budget: Budget): Observable<Budget> {
    const url = `${this.api_url_budget}/${budget.id}`;
    return this.http.delete<Budget>(url);
  }

  //update expense and budget
  update_expense(expense: Expense): Observable<Expense> {
    const url = `${this.api_url}/${expense.id}`;
    return this.http.put<Expense>(url, expense, httpOptions);
  }

  update_budget(budget: Budget): Observable<Budget> {
    const url = `${this.api_url_budget}/${budget.id}`;
    return this.http.put<Budget>(url, budget, httpOptions);
  }


  //add expense and budget
   add_expense(expense: Expense): Observable<Expense> {
    return this.http.post<Expense>(this.api_url, expense, httpOptions);
  }

   add_budget(budget: Budget): Observable<Budget> {
    return this.http.post<Budget>(this.api_url_budget, budget, httpOptions);
  }

}
