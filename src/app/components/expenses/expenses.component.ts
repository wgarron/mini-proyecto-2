import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { Budget, Expense } from 'src/app/shared/expenses';

@Component({
  selector: 'app-expenses',
  templateUrl: './expenses.component.html',
  styleUrls: ['./expenses.component.css']
})
export class ExpensesComponent {
  @Input() expense!: Expense;
  @Input() budget!: Budget;
  @Output() update_expense = new EventEmitter<Budget>();
  @Output() remove_expense = new EventEmitter<Budget>();

  @ViewChild('newExpenseForm') addForm!: NgForm;

  editableExpense = false;


}
