import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { Budget } from 'src/app/shared/expenses';
import { ExpensesService } from "../../services/expenses.service";

@Component({
  selector: 'app-budget',
  templateUrl: './budget.component.html',
  styleUrls: ['./budget.component.css']
})
export class BudgetComponent  implements OnInit {
  budget: Budget[] = []

  displayedColumns: string[] = ['monto', 'id'];
  @ViewChild('newBudgetForm') addForm!: NgForm;
  @ViewChild('editBudgetForm') editForm!: NgForm;

    editableBudget = false;

    edit_id: number | undefined = undefined;

  constructor(private ExpensesService: ExpensesService){}

   ngOnInit(): void{
    this.get_budget();
  }
  reloadCurrentPage() {
    window.location.reload();
   }

  get_budget() {
    this.ExpensesService.get_budget().subscribe((all_items) => {
      this.budget = all_items;
    });
  }

  update_budget(){
  this.ExpensesService.update_budget({ id: this.edit_id, ...this.editForm.value}).subscribe(() => {
    this.get_budget();
  })
}

delete_budget(budget: Budget){
    this.ExpensesService.delete_budget(budget).subscribe(() => {
      this.budget = this.budget.filter((i) => i.id !== budget.id);
    })
  }

  search_budget(id: any){
   this.edit_id = id;
   this.ExpensesService.get_budget_by_id(id).subscribe((budget) => {
    this.editForm.setValue({
      budgetAmout: budget.budgetAmout,
      id: budget.id
    });
   })
  }

   add_budget() {
  this.ExpensesService.add_budget(this.addForm.value).subscribe(() => {
    this.get_budget();
  })
}

}
