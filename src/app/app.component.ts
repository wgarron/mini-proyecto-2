import { Component, OnInit, ViewChild} from '@angular/core';
import { NgForm } from '@angular/forms';

import { ExpensesService } from "./services/expenses.service";
import { Expense, Budget } from "./shared/expenses";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Expense Tracker';
  @ViewChild('newExpenseForm') addForm!: NgForm;
  @ViewChild('editExpenseForm') editForm!: NgForm;
  all_expenses: Expense[] = []
  budget: Budget[] = []
  editableExpense = false;
  editableBudget = false;
  edit_id: number | undefined = undefined;

  constructor(private ExpensesService: ExpensesService ){}

  ngOnInit(): void {
     this.get_expenses();
     this.get_budget();
  }

  //get expenses and budget
   get_expenses() {
    this.ExpensesService.get_expenses().subscribe((all_items) => {
      this.all_expenses = all_items;
    });
  }

  get_budget() {
    this.ExpensesService.get_budget().subscribe((all_items) => {
      this.budget = all_items;
    });
  }

  //add expense or budget
  add_expense() {
  this.ExpensesService.add_expense(this.addForm.value).subscribe(() => {
    this.get_expenses();
  })
}

//delete expenses and budget
  delete_expense(expense: Expense) {
    this.ExpensesService.delete_expense(expense).subscribe(() => {
      this.all_expenses = this.all_expenses.filter((t) => t.id !== expense.id);
    });
  }

  //update expenses and budget
   update_expense(expense: Expense) {
    this.ExpensesService.update_expense(expense).subscribe(() => {
      this.get_expenses();
    });
  }

  get show_expenses() {
    return this.all_expenses;
  }
  get show_budget() {
    return this.budget;
  }

 get totalExpenses() {
  let sum=0;
  for(let a of this.all_expenses){
      sum=sum+a.expenseAmount;
  }
   return sum
  }

  get totalBudget() {
  let total=0;
  for(let a of this.budget){
      total=total+a.budgetAmout;
  }
   return total
  }

  get balance() {
   let remaining =  this.totalBudget -  this.totalExpenses;
   return remaining
  }

}
